package com.ivan.gestUsuario.controllers;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.sym.Name;
import com.ivan.gestUsuario.entidades.Clientes;
import com.ivan.gestUsuario.entidades.Productos;
import com.ivan.gestUsuario.repos.IProductosClientes;
import com.ivan.gestUsuario.repos.Iclientes;

@Controller
public class IndexController {

	@Autowired
	private Iclientes clienteService;


	@GetMapping({ "", "/", "/index", "/index"})
	public String index(Model model) {
		return "index";
	}
	
	@GetMapping({ "/listarClientes"})
	public String listarClientes(Model model) {
		model.addAttribute("titulo", "Listado de clientes");
		model.addAttribute("clientes", clienteService.findAll());
		return "listarClientes";
	}
	

	
	@GetMapping({ "/listarClientesExtra"})
	public String listarClientesExtra(@RequestParam(value = "nombre", defaultValue = "")String name, Model model) {
		model.addAttribute("titulo", "Listado de clientes");

		if(name.equalsIgnoreCase(""))
			model.addAttribute("clientes", clienteService.findAll());
		else
		{
			List<Clientes> aux = clienteService.findByName(name.toLowerCase());
			model.addAttribute("clientes", aux );	
		}

		return "listarClientesExtra";
	}
	
	
	@GetMapping({ "/listarClientesSinUsuario"})
	public String listarClientesSinUsuarop(Model model) {
		model.addAttribute("titulo", "Listado de clientes");

		model.addAttribute("clientes", clienteService.findCliNoUser());

		return "listarClientes";
	}
	

	
	@GetMapping("/agregarCliente" )
	public String agregarCliente(Model model)
	{
		model.addAttribute("cliente", new Clientes());
		model.addAttribute("titulo", "Crear nuevo cliente");
		model.addAttribute("action", "/createCliente");
		return "agregarCliente";
	}
	

	
	@GetMapping("/exito" )
	public String exito( Model model)
	{
		return "exito";
	}
	

	
	@RequestMapping("/createCliente")
    public String createCliente(@Valid Clientes user, BindingResult result) {
        user.setCreateAt(new Date());
        try
        {
        	clienteService.save(user);
        }  catch(DataAccessException e) { return "/error"; }
        return result.hasErrors() ? "/error" : "exito";
    }
	
	@GetMapping("/borrarCliente" )
	public String borrarCliente(Model model)
	{
		model.addAttribute("cliente", new Clientes());
		model.addAttribute("titulo", "Borrado de usuario");
		model.addAttribute("action", "/deleteCliente");
		model.addAttribute("accion", "Usuario a borrar");

		model.addAttribute("method", "post");
		return "borrarCliente";
	}
	
	@RequestMapping("/deleteCliente")
    public ModelAndView deleteCliente(@Valid Clientes user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error");
        if(!clienteService.existsById(user.getId()))
        	return model;
        try
        {
        	clienteService.delete(user);
            model.setViewName(result.hasErrors() ? "error" : "exito"); 

        } catch (Exception e) { 
        	model.setViewName("error");
        	return model; 
        	}
        model.setViewName("exito");
        return model;
    }
	
	@GetMapping("/modificarCliente")
	public String modificarCliente(Model model)
	{
		model.addAttribute("cliente", new Clientes());
		model.addAttribute("method", "post");
		model.addAttribute("action", "/modCliente");
		return "modificarCliente";
	}
	
	
	@RequestMapping("/modCliente")
    public String modUser(@Valid Clientes user, BindingResult result) {
        
        Clientes aux = clienteService.findById(user.getId()).orElse(null);
        if(aux == null)
        	return "error";
        
        if(!user.getApellido().equals(""))
        	aux.setApellido(user.getApellido());
        if(!user.getNombre().equals(""))
        	aux.setNombre(user.getNombre());
        if(!user.getEmail().equals(""))
        	aux.setEmail(user.getEmail());
        
        clienteService.save(aux);
        return "exito";
    }
	
}
