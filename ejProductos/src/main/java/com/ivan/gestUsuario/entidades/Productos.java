package com.ivan.gestUsuario.entidades;
// Generated 25-ene-2020 3:30:09 by Hibernate Tools 5.4.7.Final

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Productos generated by hbm2java
 */
@Entity
public class Productos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8202819542078200454L;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int codproducto;
	@NotEmpty(message = "No puede estar vacio")
	private String descripcion;
	@NotNull(message = "No puede estar vacio")
	private Float precio;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date fechaAlta;
	private Boolean disponibilidad;
	
	@OneToMany(mappedBy = "codProducto")
	private Set<ProductosClientes> productosClienteses = new HashSet<ProductosClientes>();

	public Productos() {
	}

	public Productos(int codproducto) {
		this.codproducto = codproducto;
		System.out.println("eyyy productos " + codproducto);
	}

	public Productos(int codproducto, String descripcion, Float precio, Date fechaAlta, Boolean disponibilidad,
			Set<ProductosClientes> productosClienteses) {
		this.codproducto = codproducto;
		this.descripcion = descripcion;
		this.precio = precio;
		this.fechaAlta = fechaAlta;
		this.disponibilidad = disponibilidad;
		this.productosClienteses = productosClienteses;
	}

	public int getCodproducto() {
		return this.codproducto;
	}

	public void setCodproducto(int codproducto) {
		this.codproducto = codproducto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Float getPrecio() {
		return this.precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Date getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Boolean getDisponibilidad() {
		return this.disponibilidad;
	}

	public void setDisponibilidad(Boolean disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public Set<ProductosClientes> getProductosClienteses() {
		return this.productosClienteses;
	}

	public void setProductosClienteses(Set<ProductosClientes> productosClienteses) {
		this.productosClienteses = productosClienteses;
	}

}
