package com.ivan.gestUsuario.repos;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Service;

import com.ivan.gestUsuario.entidades.ProductosClientes;
import com.ivan.gestUsuario.entidades.ProductosClientesId;

@RepositoryRestResource(path = "productosClientes", collectionResourceRel = "productosClientes" )
public interface IProductosClientes extends JpaRepository<ProductosClientes, ProductosClientesId>{

}
