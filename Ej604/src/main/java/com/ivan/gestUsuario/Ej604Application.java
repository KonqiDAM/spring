package com.ivan.gestUsuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ej604Application {

	public static void main(String[] args) {
		SpringApplication.run(Ej604Application.class, args);
	}

}
