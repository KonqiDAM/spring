package com.ivan.gestUsuario.controllers;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ivan.gestUsuario.entidades.Clientes;
import com.ivan.gestUsuario.entidades.Productos;
import com.ivan.gestUsuario.repos.IProductos;

@Controller
public class ProductoController {

	@Autowired
	private IProductos productoService;
	
	@GetMapping({ "/listarProductos"})
	public String listarProductos(Model model) {
		model.addAttribute("titulo", "Listado de productos");
		model.addAttribute("productos", productoService.findAll());
		return "listarProductos";
	}
	
	@GetMapping("/agregarProducto" )
	public String agregarProducto(Model model)
	{
		model.addAttribute("cliente", new Productos());
		model.addAttribute("titulo", "Crear nuevo producto");
		model.addAttribute("action", "/createProducto");
		return "agregarProducto";
	}
	
	@RequestMapping("/createProducto")
    public String createProducto(@Valid Productos user, BindingResult result) {
        user.setFechaAlta(new Date());
        productoService.save(user);
        return result.hasErrors() ? "redirect:/error" : "redirect:/exito";
    }
	
	@GetMapping("/borrarProducto" )
	public String borrarProducto(Model model)
	{
		model.addAttribute("cliente", new Productos());
		model.addAttribute("titulo", "Borrado de producto");
		model.addAttribute("action", "/deleteProducto");
		model.addAttribute("accion", "producto a borrar");

		model.addAttribute("method", "post");
		return "borrarProducto";
	}
	
	@RequestMapping("/deleteProducto")
    public ModelAndView deleteProducto(@Valid Productos user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error");
        if(!productoService.existsById(user.getCodproducto()))
        	return model;
        try
        {
        	productoService.delete(user);
            model.setViewName(result.hasErrors() ? "error" : "exito"); 

        } catch (Exception e) { }
        model.setViewName("exito");

        return model;
    }
	
	
	@GetMapping("/modificarProducto")
	public String modificarProducto(Model model)
	{
		model.addAttribute("cliente", new Productos());
		model.addAttribute("method", "post");
		model.addAttribute("action", "/modProducto");
		return "modificarProducto";
	}
	
	
	@RequestMapping("/modProducto")
    public ModelAndView modUser(@Valid Productos user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error"); 
        
        Productos aux = productoService.findById(user.getCodproducto()).orElse(null);
        if(aux == null)
        	return model;
        model.setViewName("exito"); 

        if(!user.getDescripcion().equals(""))
        	aux.setDescripcion(user.getDescripcion());
        if(user.getPrecio() != null)
        	aux.setPrecio(user.getPrecio());
        
        productoService.save(aux);
        return model;
    }
}
