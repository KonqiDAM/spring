package com.ivan.apirest.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ivan.apirest.models.entity.Clientes;

public interface IClientes extends JpaRepository<Clientes, Integer>{

}
