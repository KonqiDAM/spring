package com.ivan.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.ivan.apirest.models.entity.Cliente;

public interface IclienteDAO extends CrudRepository<Cliente, Long> {

}
