package com.ivan.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ivan.demo.models.User;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
	@GetMapping ({"/index", "", "/home"} )
	public String index(Model model)
	{
		//
		model.addAttribute("titulo", textoIndex);
		return "index";
	}
	
	@RequestMapping(value = "/perfil")
	public String perfil(Model model)
	{
		User us = new User("Ivan", "Lazcano", "asd@asd.com");
		model.addAttribute("usuario", us);
		model.addAttribute("titulo", textoPerfil.concat(us.getName()));
		return "perfil";
	}
	
	@RequestMapping(value = "/listar")
	public String listar(Model model)
	{
		model.addAttribute("titulo", textoListar);
		return "listar";
	}
	
	
	@ModelAttribute("usuarios")
	public List<User> poblarUsers()
	{
		List<User> usuarios = new ArrayList();
		usuarios.add(new User("U1", "AP2", "asdasd@asd.com"));
		usuarios.add(new User("U2", "AP3", "asdas123d@asd.com"));
		usuarios.add(new User("U2", "AP4", "asdasd123@asd.com"));
		return usuarios;
	}

}
