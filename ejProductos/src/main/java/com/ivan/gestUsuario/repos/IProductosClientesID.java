package com.ivan.gestUsuario.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ivan.gestUsuario.entidades.ProductosClientes;
import com.ivan.gestUsuario.entidades.ProductosClientesId;

public interface IProductosClientesID extends JpaRepository<ProductosClientes, ProductosClientesId> {

}
