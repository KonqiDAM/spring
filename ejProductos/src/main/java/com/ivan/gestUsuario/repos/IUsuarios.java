package com.ivan.gestUsuario.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ivan.gestUsuario.entidades.ComprasUsuario;
import com.ivan.gestUsuario.entidades.IComprasUsuario;
import com.ivan.gestUsuario.entidades.Usuarios;

@RepositoryRestResource(path = "usuarios", collectionResourceRel = "usuarios")
public interface IUsuarios extends JpaRepository<Usuarios, Long> {

	@Query(value = "select fecha, descripcion from usuarios u, productos_clientes v, productos p where v.cod_cliente = u.idcliente and v.cod_producto = p.codproducto and u.id = ?1", nativeQuery = true)
	public List<IComprasUsuario> findSalesById(@Param("id") Long id);
}
