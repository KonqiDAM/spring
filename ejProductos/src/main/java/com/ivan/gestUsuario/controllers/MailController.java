package com.ivan.gestUsuario.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ivan.gestUsuario.entidades.Clientes;
import com.ivan.gestUsuario.entidades.Mails;
import com.ivan.gestUsuario.entidades.Usuarios;
import com.ivan.gestUsuario.repos.IMails;
import com.ivan.gestUsuario.repos.Iclientes;

@Controller
public class MailController {

	@Autowired
	private IMails mailsService;

	
	@GetMapping({ "/listarMails"})
	public String listarMails(Model model) {
		model.addAttribute("titulo", "Listado de correos");
		model.addAttribute("mails", mailsService.findAll());
		return "listarMails";
	}
	
	
	@GetMapping("/agregarMail")
	public String agregarMail(Model model) {
		model.addAttribute("mail", new Mails());
		model.addAttribute("titulo", "Crear nuevo Email");
		model.addAttribute("action", "/createMail");
		return "agregarMail";
	}

	@RequestMapping("/createMail")
	public String createMail(@Valid Mails mail, BindingResult result) {
		try {
			mailsService.save(mail);
		} catch (DataAccessException e) {
			return "/error";
		}
		return result.hasErrors() ? "/error" : "exito";
	}
	
	
	@GetMapping("/modificarMail")
	public String modificarMail(Model model)
	{
		model.addAttribute("mail", new Mails());
		model.addAttribute("method", "post");
		model.addAttribute("action", "/modMail");
		return "modificarMail";
	}
	
	
	@RequestMapping("/modMail")
    public String modUser(@Valid Mails oldMail, BindingResult result) {
        
		Mails aux = mailsService.findById(oldMail.getId()).orElse(null);
        if(aux == null)
        	return "error";
        
        if(!oldMail.getMail().equals(""))
        	aux.setMail(oldMail.getMail());
        if(oldMail.getUsuarios() != null )
        	aux.setUsuarios(oldMail.getUsuarios());
       
        
        mailsService.save(aux);
        return "exito";
    }

}
