package com.ivan.gestUsuario.controllers;

import java.awt.List;
import java.util.ArrayList;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ivan.gestUsuario.entidades.Clientes;
import com.ivan.gestUsuario.entidades.ProductosClientes;
import com.ivan.gestUsuario.entidades.ProductosClientesId;
import com.ivan.gestUsuario.entidades.VentasList;
import com.ivan.gestUsuario.repos.IProductosClientes;

@Controller
public class VentasController {

	@Autowired
	private IProductosClientes producotClienteService;
	
	@GetMapping({ "/listarVentas"})
	public String listarVentas(Model model) {
		model.addAttribute("titulo", "Listado de ventas");
		model.addAttribute("ventas", producotClienteService.findAll());
		return "listarVentas";
	}
	
	@GetMapping("/agregarVenta" )
	public String agregarVenta(Model model)
	{
		model.addAttribute("cliente", new ProductosClientes());
		model.addAttribute("titulo", "Crear nueva venta");
		model.addAttribute("action", "/createVenta");
		return "agregarVenta";
	}
	
	
	@RequestMapping("/createVenta")
    public String createVenta(@Valid ProductosClientes user, BindingResult result) {
        user.setFecha(new Date());
        producotClienteService.save(user);
        return result.hasErrors() ? "redirect:/error" : "redirect:/exito";
    }
	
	@GetMapping("/borrarVenta" )
	public String borrarVenta(Model model)
	{
		model.addAttribute("cliente", new ProductosClientes());
		model.addAttribute("titulo", "Borrado de venta");
		model.addAttribute("action", "/deleteVenta");
		model.addAttribute("accion", "Venta a borrar");

		model.addAttribute("method", "post");
		return "borrarVenta";
	}
	
	@RequestMapping("/deleteVenta")
    public String deleteVenta(@Valid ProductosClientes user, BindingResult result) {
		ProductosClientesId id = new ProductosClientesId();
        id.setCodCliente(user.getCodCliente());
        id.setCodProducto(user.getCodProducto());
        id.setFecha(user.getFecha());
        ProductosClientes aux = producotClienteService.findById(id).orElse(null);
        if(aux == null)
        	return "error";
        try
        {
        	producotClienteService.delete(user);

        } catch (Exception e) { return "error"; }
        return "exito";
    }
	
	@GetMapping("/modificarVenta")
	public String modificarCliente(Model model)
	{
		VentasList ventas = new VentasList();
		ventas.add(new ProductosClientes());
		ventas.add(new ProductosClientes());
		model.addAttribute("ventas", ventas);

		model.addAttribute("method", "post");
		model.addAttribute("action", "/modVenta");
		return "modificarVenta";
	}
	
	@Transactional
	@RequestMapping("/modVenta")
    public ModelAndView modUser(@Valid VentasList ventas, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error"); 
        
        ProductosClientes nuevo = ventas.getVentas().get(1);
        ProductosClientesId id = new ProductosClientesId();
        id.setCodCliente(ventas.getVentas().get(0).getCodCliente());
        id.setCodProducto(ventas.getVentas().get(0).getCodProducto());
        id.setFecha(ventas.getVentas().get(0).getFecha());

        ProductosClientes aux = producotClienteService.findById(id).orElse(null);
        if(aux == null)
        	return model;
        
        if(nuevo.getCodCliente() == null)
        	nuevo.setCodCliente(aux.getCodCliente());
        if(nuevo.getCodProducto() == null)
        	nuevo.setCodProducto(aux.getCodProducto());
        if(nuevo.getFecha() == null)
        	nuevo.setFecha(aux.getFecha());
        
        try
        {
        	id.setCodCliente(aux.getCodCliente());
        	id.setCodProducto(aux.getCodProducto());
        	id.setFecha(aux.getFecha());
        	System.out.println("AQUI1");
        	if(producotClienteService.findById(id).orElse(null) != null)
        	{
	            System.out.println("AQUI2");
	        	producotClienteService.save(nuevo);
	            System.out.println("AQUI3");

	            producotClienteService.delete(aux);


	            System.out.println("AQUI4");
        	}
        	else
        		model.setViewName("error");

        } catch(Exception e) { model.setViewName("error"); }
        model.setViewName(result.hasErrors() ? "error" : "exito"); 
        return model;
    }
}