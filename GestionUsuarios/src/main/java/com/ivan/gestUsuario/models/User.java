package com.ivan.gestUsuario.models;

import java.util.Comparator;

import ch.qos.logback.core.subst.Token.Type;

public class User {
	
	private static int actualID = 0;
	
	private int id;
	private String name;
	private String email;
	private String password;
	
	public User()
	{
		
	}
	
	public User(String name, String email, String password) {
		super();
		this.id = actualID;
		actualID++;
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public User(int id, String name, String email, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public static int asignID(User us)
	{
		return actualID++;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof User && ((User)o).id == this.id)
			return true;
		return false;
		
	}
	
}
