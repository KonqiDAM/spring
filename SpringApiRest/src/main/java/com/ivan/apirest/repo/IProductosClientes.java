package com.ivan.apirest.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ivan.apirest.models.entity.ProductosClientes;

public interface IProductosClientes extends JpaRepository<ProductosClientes, Integer> {

}
