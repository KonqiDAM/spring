package com.ivan.gestUsuario.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ivan.gestUsuario.entidades.Mails;
@RepositoryRestResource(path = "mails", collectionResourceRel = "mails" )
public interface IMails extends JpaRepository<Mails, Long> {

}
