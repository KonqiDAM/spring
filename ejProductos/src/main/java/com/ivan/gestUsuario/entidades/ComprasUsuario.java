package com.ivan.gestUsuario.entidades;

import java.util.Date;

public class ComprasUsuario {
	Date fecha;
	String decripcion;

	public ComprasUsuario() {
		super();
	}



	public ComprasUsuario(Date fecha, String decripcion) {
		super();
		this.fecha = fecha;
		this.decripcion = decripcion;
	}



	public Date getFecha() {
		return fecha;
	}



	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public String getDecripcion() {
		return decripcion;
	}

	public void setDecripcion(String decripcion) {
		this.decripcion = decripcion;
	}

}
