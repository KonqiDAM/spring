package com.ivan.gestUsuario.controllers;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ivan.gestUsuario.entidades.*;
import com.ivan.gestUsuario.entidades.Mails;
import com.ivan.gestUsuario.entidades.Usuarios;
import com.ivan.gestUsuario.repos.IMails;
import com.ivan.gestUsuario.repos.IUsuarios;

@Controller
public class UsuariosController {

	
	@Autowired
	private IUsuarios usuariosService;
	
	@Autowired
	private IMails mailsService;
	
	

	
	
	@GetMapping({ "/listarUsuarios"})
	public String listarUsuarios(Model model) {
		model.addAttribute("titulo", "Listado de usuarios");
		model.addAttribute("usuarios", usuariosService.findAll());
		return "listarUsuarios";
	}
	
	@GetMapping({ "/listarCompras"})
	public String listarCompras(@RequestParam(value = "id", defaultValue = "-1")Long id, Model model) {
		Usuarios aux = usuariosService.findById(id).orElse(null);
		List<IComprasUsuario> c = usuariosService.findSalesById(id);
		model.addAttribute("usuario", aux );
		model.addAttribute("compras", c );
		model.addAttribute("titulo", "Listado de usuarios");
		return "listarCompras";
	}
	
	@GetMapping("/agregarUsuario" )
	public String agregarUsuario(Model model)
	{
		model.addAttribute("usuario", new Usuarios());
		model.addAttribute("mail", new Mails());
		model.addAttribute("titulo", "Crear nuevo usuario");
		model.addAttribute("action", "/createUsuario");
		return "agregarUsuario";
	}
	
	
	@RequestMapping("/createUsuario")
    public String createUsuario(@Valid Usuarios usuario, Mails email,  BindingResult result) {
        try
        {
        	Usuarios auxUsu = usuariosService.save(usuario);
        	email.setUsuarios(auxUsu);
        	mailsService.save(email);
        }  catch(DataAccessException e) { return "/error"; }
        return result.hasErrors() ? "/error" : "exito";
    }
	
	@GetMapping("/borrarUsuario" )
	public String borrarUsuario(Model model)
	{
		model.addAttribute("usuario", new Usuarios());
		model.addAttribute("titulo", "Borrado de usuario");
		model.addAttribute("action", "/deleteUsuario");
		model.addAttribute("accion", "usuario a borrar");

		model.addAttribute("method", "post");
		return "borrarUsuario";
	}
	
	@RequestMapping("/deleteUsuario")
    public ModelAndView deleteUsuario(@Valid Usuarios user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error");
        if(!usuariosService.existsById(user.getId()))
        	return model;
        try
        {
        	usuariosService.delete(user);
            model.setViewName(result.hasErrors() ? "error" : "exito"); 

        } catch (Exception e) { }
        model.setViewName("exito");
        return model;
    }
	
	@RequestMapping("/modificarUsuario")
	public String modificarUsuario(Model model)
	{
		model.addAttribute("usuario", new Usuarios());
		model.addAttribute("titulo", "Modificar usuario");
		model.addAttribute("method", "post");
		model.addAttribute("action", "/modUsuario");
		return "modificarUsuario";
	}
	
	@RequestMapping("/modUsuario")
    public String modUsuario(@Valid Usuarios user, BindingResult result) {
        
        Usuarios aux = usuariosService.findById(user.getId()).orElse(null);
        if(aux == null)
        	return "error";
		System.out.println("AUX ID: " + aux.getClientes().getId());

        if(!user.getPassword().equals(""))
        	aux.setPassword(user.getPassword());
        if(user.getClientes() != null)
        	aux.setClientes(user.getClientes());
        if(!user.getUsuario().equals(""))
        	aux.setUsuario(user.getUsuario());
        
		System.out.println(aux.getClientes().getId());

        usuariosService.save(aux);
        return "exito";
    }
	
	@GetMapping("/test")
	public List<Clientes> test()
	{
		//return usuariosServiceAux.findClientesSinUsuario();
		return null;
	}
}
