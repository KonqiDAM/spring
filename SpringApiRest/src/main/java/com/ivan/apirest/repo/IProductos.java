package com.ivan.apirest.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ivan.apirest.models.entity.Productos;

public interface IProductos extends JpaRepository<Productos, Integer> {

}
