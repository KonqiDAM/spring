package com.ivan.gestUsuario.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ivan.gestUsuario.entidades.Productos;

@RepositoryRestResource(path = "productos", collectionResourceRel = "productos" )
public interface IProductos extends JpaRepository<Productos, Integer>{

}
