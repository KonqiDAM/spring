package com.ivan.gestUsuario.entidades;
// Generated 25-ene-2020 3:30:09 by Hibernate Tools 5.4.7.Final

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * ProductosClientes generated by hbm2java
 */
@Entity
@IdClass(ProductosClientesId.class)
@QuerydslPredicate
public class ProductosClientes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    @DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Id
	private Date fecha;
	@Id
	private Long codCliente;
	@Id
	private Long codProducto;
	

	public ProductosClientes() {
	}

	public ProductosClientes(Date fecha, Long codCliente, Long codProducto) {
		super();
		this.fecha = fecha;
		this.codCliente = codCliente;
		this.codProducto = codProducto;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Long getCodCliente() {
		return codCliente;
	}


	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}


	public Long getCodProducto() {
		return codProducto;
	}


	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}
	
}
