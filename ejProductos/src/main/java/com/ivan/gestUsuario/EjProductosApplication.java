package com.ivan.gestUsuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjProductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjProductosApplication.class, args);
	}
}