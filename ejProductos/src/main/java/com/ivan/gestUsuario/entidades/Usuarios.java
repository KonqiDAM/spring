package com.ivan.gestUsuario.entidades;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Usuarios implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	private Clientes clientes;
	@NotEmpty(message = "No puede estar vacio")
	@Size(min = 4, max = 12, message = "el tamaño tiene que estar entre 4 y 12")
	private String usuario;
	@NotEmpty(message = "No puede estar vacio")
	@Size(min = 4, max = 12, message = "el tamaño tiene que estar entre 4 y 12")
	private String password;

	@OneToMany(mappedBy = "usuarios")
	private Set<Mails> mails = new HashSet<Mails>();
	
	public Usuarios() {
	}

	public Usuarios(Long id) {
		this.id = id;
	}

	public Usuarios(Long id, Clientes clientes, String usuario, String password) {
		this.id = id;
		this.clientes = clientes;
		this.usuario = usuario;
		this.password = password;
	}
	
	public Long getIdcliente() {
		
		return clientes.getId();
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Clientes getClientes() {
		return this.clientes;
	}

	public void setClientes(Clientes clientes) {
		this.clientes = clientes;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Mails> getMails() {
		return mails;
	}
	
	public ArrayList<Mails> getMailsArray()
	{
		return  new ArrayList<Mails>(mails);
	}

	public void setMails(Set<Mails> mails) {
		this.mails = mails;
	}
	
	

}