package com.ivan.gestUsuario.entidades;

import java.util.ArrayList;

public class VentasList
{
	ArrayList<ProductosClientes> ventas;
	
	public VentasList()
	{
		ventas = new ArrayList<>();
	}
	
	public void add(ProductosClientes v)
	{
		ventas.add(v);
	}
	
	public ArrayList<ProductosClientes> getVentas() {
		return ventas;
	}
}