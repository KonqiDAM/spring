package com.ivan.gestUsuario.entidades;

import java.util.Date;

public interface IComprasUsuario {
	String getDescripcion();
	Date getFecha();
}

