package com.ivan.gestUsuario.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ivan.gestUsuario.models.User;


@Controller
public class IndexController {

	private List<User> usuarios;
	
	
	public IndexController() {
		usuarios = new ArrayList();
		usuarios.add(new User("U1", "asdasd@asd.com", "123asd"));
		usuarios.add(new User("U2", "asdasd2@asd.com", "123asd"));
		usuarios.add(new User("U3", "asdasd3@asd.com", "123asd"));
	}
	
	
	@GetMapping({"","/", "/index", "/index/"} )
	public String index(Model model)
	{
		model.addAttribute("titulo", "Listado de acciones");
		return "index";
	}
	
	@RequestMapping(value = "/listar")
	public String listar(Model model)
	{
		model.addAttribute("titulo", "Listado de usuarios");
		model.addAttribute("usuarios", usuarios);
		return "listar";
	}
	
	@GetMapping("/agregar" )
	public String agregar(Model model)
	{
		model.addAttribute("usuario", new User());
		model.addAttribute("titulo", "Crear nuevo usuario");
		model.addAttribute("action", "/create");
		return "agregar";
	}
	
	@GetMapping("/borrar" )
	public String borrar(Model model)
	{
		model.addAttribute("usuario", new User());
		model.addAttribute("titulo", "Borrado de usuario");
		model.addAttribute("action", "/delete");
		model.addAttribute("accion", "Usuario a borrar");

		model.addAttribute("method", "post");
		return "borrar";
	}
	
	@GetMapping("/modificar" )
	public String modificar(Model model)
	{
		model.addAttribute("usuario", new User());
		model.addAttribute("titulo", "Modifiacacion de usuario");
		model.addAttribute("action", "/modify");
		model.addAttribute("accion", "Usuario a cambiar");

		model.addAttribute("method", "get");	
		return "borrar";
	}
	
	
	@GetMapping("/modify")
	public String modify(@RequestParam int id,  Model model)
	{
		for (User u : usuarios) {
			if(u.getId() == id)
				model.addAttribute("usuario", u);
		}
		model.addAttribute("titulo", id);
		model.addAttribute("action", "/mod");
		return "agregar";
	}
	
	@RequestMapping("/create")
    public ModelAndView createUser(@Valid User user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        user.setId(User.asignID(user));
        usuarios.add(user);
        model.setViewName(result.hasErrors() ? "error" : "exito"); 
        return model;
    }
	
	@RequestMapping("/delete")
    public ModelAndView deleteUser(@Valid User user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        usuarios.remove(user);
        model.setViewName(result.hasErrors() ? "error" : "exito"); 
        return model;
    }
	
	@RequestMapping("/mod")
    public ModelAndView modUser(@Valid User user, BindingResult result) {
        ModelAndView model = new ModelAndView();
        usuarios.remove(user);
        usuarios.add(user);
        model.setViewName(result.hasErrors() ? "error" : "exito"); 
        return model;
    }
}
