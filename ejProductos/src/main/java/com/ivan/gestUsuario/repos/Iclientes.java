package com.ivan.gestUsuario.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ivan.gestUsuario.entidades.Clientes;

@RepositoryRestResource(path = "clientes", collectionResourceRel = "clientes")
public interface Iclientes extends JpaRepository<Clientes, Long> {

	@Query(value = "select * from clientes c where NOT EXISTS (SELECT c.* FROM  usuarios u where u.idcliente = c.id);", nativeQuery = true)
	List<Clientes> findCliNoUser();
	
	@Query(value = "select * from clientes where lower(nombre) like %?1%", nativeQuery = true)
	List<Clientes> findByName(@Param("nombre") String nombre);
}
