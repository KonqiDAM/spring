package com.ivan.demo.models;

public class User {

	String name;
	String forename;
	String email;
	
	public User()
	{
		
	}
	public User(String name, String forename) {
		super();
		this.name = name;
		this.forename = forename;
	}
	
	public User(String name, String forename, String email) {
		super();
		this.name = name;
		this.forename = forename;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForename() {
		return forename;
	}
	public void setForename(String forename) {
		this.forename = forename;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
