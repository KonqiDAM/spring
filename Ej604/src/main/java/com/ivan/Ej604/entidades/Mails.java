package com.ivan.Ej604.entidades;
// Generated 06-feb-2020 19:58:30 by Hibernate Tools 5.4.7.Final

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Mails generated by hbm2java
 */
@Entity
public class Mails implements java.io.Serializable {

	@Id
	@GeneratedValue
	private int id;
	@ManyToOne
	private Usuarios usuarios;
	private String mail;

	public Mails() {
	}

	public Mails(int id, String mail) {
		this.id = id;
		this.mail = mail;
	}

	public Mails(int id, Usuarios usuarios, String mail) {
		this.id = id;
		this.usuarios = usuarios;
		this.mail = mail;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuarios getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
